# gitlab初始化准备工作

版本：v1.0

时间：2019-01-30

[TOC]

## 创建群组

一般产品开发时，创建一个产品Group，再分别创建至少3个工程，前端工程，后端工程，文档工程

可以在Group创建issue&merge模板和标签，可以在子工程中继承

##  创建工程

创建项目有3种访问权限

- private 完全私有的，如果要添加用户必须手工添加  **//一般产品研发项目用这种**

- internal 内部使用，这里指的是登录gitlab的用户，就是登录认证通过的

- public 完全公开 随意访问  **//公司级别内部共享的框架项目用这种**

  

当项目为private的时候，添加用户有几种角色

- Master   
- Developer
- Reportor
- Guest   //没有什么权限，代码都看不见，只能看看wiki和一些琐碎的东西

具体权限可以看这里： <http://fitnesstest.diankexuexi.com:10080/help/user/permissions>

按照权限要求，添加用户

## 创建issue&merge模板

创建步骤：

- 在根目录下创建 [.gitlab](http://fitnesstest.diankexuexi.com:10080/CT/training/tree/master/.gitlab) 目录 
- 在[.gitlab](http://fitnesstest.diankexuexi.com:10080/CT/training/tree/master/.gitlab) 目录创建issue_templates目录和merge_request_templates目录
- issue_templates目录分别放入：feature issue templates，bug issue templates，document issue templates，test issue templates，management issue templates
- merge_request_templates目录分别放入：feature merge request templates，bug merge request templates，product merge request templates

在创建Issue时根据任务类型，选择不同的模板

![2](https://i.loli.net/2019/02/13/5c62ee49a6e9e.jpg)

## Issue初始化label

issue里面还有一个label需要配置，从任务类型，完成状态，紧急程度，几个维度描述，对于一个issue并不是所有维度标签都是必需的：**类型（必需），紧急程度（可选），完成状态（必需）**

**类型**

- feature  新功能开发任务  //#00A8DF
- bug  修复bug任务  //#FF3737
- document 文档任务  //#D1D100
- testing 测试任务   //#5CB85C
- management  管理任务 其他类型都算到这里   //#44AD8E
- MR feature   新功能分支合并  //#5843AD
- MR bug  修复bug任务  //#D10069
- MR product 文档任务  //#AD8D43

**紧急程度**

- normal  一般不紧急 可稍缓    //#00A8DF
- concerned  需要关注 尽快解决的   //#FF6D0D
- critical 非常严重  立即处理     // #FF3737

**完成状态**

- todo  准备做的 尚未开始  //#7F8C8D

- processing  正在处理   //#00A8DF

- pending 挂起 暂时挂起稍后处理 //#34495E

- acceptance 已完成 待验收 //#44AD8E

  


![1](https://i.loli.net/2019/02/13/5c62ee09a9b5f.jpg)

## 创建Milestones

issue里面创建Milestones，将所有Issue按照Milestones进行分组管理

## 创建Product分支

默认工程创建完成，就会有Master分支，但是按照分支开发的原则，还需要有其他分支

- master 长期分支  开发在此进行，包括持续集成
- release 分支 只保存里程碑版本 不对其进行开发操作
- feature & fixbug 短期分支 feature是新需求 开发人员根据feature issue开分支，fixbug是bug 开发人员根据bug issue开分支 完成后删除

master，release分支必须设置分支保护，禁止直接提交

​        